@usecase=all
Feature: Consulta de ingresos y gastos en el mes en curso
  @usecase=case1
  Scenario: Quiero conocer cual es el total de mis Gastos y cual es el total de mis Ingresos calculados en el mes en curso.
    Given a book exists with an isbn of 9781451648546
    When a user retrieves the book by isbn
    Then the status code is 404
  @usecase=case2
  Scenario: Test2
    Given a book exists with an isbn of 9781451648546
    When a user retrieves the book by isbn
    Then the status code is 200