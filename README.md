# Getting started with Serenity and Cucumber 2.4

Serenity BDD is a library that makes it easier to write high quality automated acceptance tests, with powerful reporting and living documentation features. It has strong support for both web testing with Selenium, and API testing using RestAssured. 

Serenity strongly encourages good test automation design, and supports several design patterns, including classic Page Objects, the newer Lean Page Objects/ Action Classes approach, and the more sophisticated and flexible Screenplay pattern.

The latest version of Serenity supports both Cucumber 2.4 and the more recent Cucumber 4.x. Cucumber 4 is not backward compatible with Cucumber 2. If you would like to learn more about Serenity and Cucumber 4, take a look at [the Serenity BDD/Cucumber 4 Starter Project](https://github.com/serenity-bdd/serenity-cucumber4-starter).

## The starter project

The best place to start with Serenity and Cucumber is to clone or download the starter project on Github ([https://github.com/serenity-bdd/serenity-cucumber-starter](https://github.com/serenity-bdd/serenity-cucumber-starter)). This project gives you a basic project setup, along with some sample tests and supporting classes. There are two versions to choose from. The master branch uses a more classic approach, using action classes and lightweight page objects, whereas the **[screenplay](https://github.com/serenity-bdd/serenity-cucumber-starter/tree/screenplay)** branch shows the same sample test implemented using Screenplay.

### How to use this project ###

We could use the library simply executing the following command.

	mvn clean verify


	mvn clean verify -Dcucumber.options="--tags @color=red"    
	
	
TAGS : https://johnfergusonsmart.com/running-serenity-bdd-tests-with-tags/